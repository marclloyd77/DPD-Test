'use strict';
angular.module('dpdTestApp')
    .controller('CommentCtrl', function ($scope, user, post, $routeParams, comment){
        user.get({id: $routeParams.userId}).$promise.then(function(data){
            $scope.user = data;
        });
        post.get({id: $routeParams.postId}).$promise.then(function(data){
            $scope.post = data;
        });
        comment.query({id: $routeParams.postId}).$promise.then(function(data){
            $scope.comments = data;
        });
    });