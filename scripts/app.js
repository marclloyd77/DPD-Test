'use strict';

angular
    .module('dpdTestApp', [
        'ngResource',
        'ngRoute',
        'ngAnimate'
    ])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'scripts/modules/user/users.html',
                controller: 'UserCtrl'
            })
            .when('/posts/:id', {
                templateUrl: 'scripts/modules/post/posts.html',
                controller: 'PostCtrl'
            })
            .when('/post/:userId/comments/:postId', {
                templateUrl: 'scripts/modules/comment/comments.html',
                controller: 'CommentCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });
    });